package de.neuchels.webshop.backend.model

data class ShoppingCartResponse (
    val customerId: String,
    val orderPositions: List<OrderPositionResponse>,
    val totalAmountInCent: Long,
    val deliveryCostInCent: Long,
    val deliveryOption: String
    ) {
}
