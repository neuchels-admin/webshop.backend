package de.neuchels.webshop.backend.model

data class OrderPositionCreateRequest(
    val productId: String,
    val quantity: Long
)