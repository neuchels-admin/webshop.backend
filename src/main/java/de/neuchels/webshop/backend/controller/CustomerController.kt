package de.neuchels.webshop.backend.controller

import de.neuchels.webshop.backend.model.CustomerResponse
import de.neuchels.webshop.backend.model.ShoppingCartResponse
import de.neuchels.webshop.backend.repository.CustomerRepository
import de.neuchels.webshop.backend.service.ShoppingCartService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class CustomerController(
    val customerRepository: CustomerRepository,
    val shoppingCartService: ShoppingCartService
) {
    @GetMapping("/customers/{id}")
    fun getCustomerById(
        @PathVariable id: String
    ): CustomerResponse {
        val customerEntity = customerRepository.getById(id)
        return CustomerResponse(
            id = customerEntity.id,
            firstName = customerEntity.firstName,
            lastName = customerEntity.lastName,
            email = customerEntity.email
        )
    }

    @GetMapping("/customers/{id}/shoppingcart")
    fun getShoppingCardById(
        @PathVariable id: String
    ): ShoppingCartResponse {
        return shoppingCartService.getShoppingCartByCustomerId(id)
    }
}