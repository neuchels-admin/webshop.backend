create table order_positions
(
    id         varchar(36)  not null,
    order_id   varchar(255) not null,
    product_id varchar(255) not null,
    quantity   bigint       not null,
    primary key (id)
);

create table orders
(
    id          varchar(36)  not null,
    customer_id varchar(255) not null,
    order_time  timestamp    not null,
    status      varchar(255) not null,
    primary key (id)
);

alter table order_positions
    add constraint FK_order_positions_orders
        foreign key (order_id)
            references orders (id);

alter table order_positions
    add constraint FK_order_positions_products
        foreign key (product_id)
            references products (id);

alter table orders
    add constraint FK_orders_customers
        foreign key (customer_id)
            references customers (id);