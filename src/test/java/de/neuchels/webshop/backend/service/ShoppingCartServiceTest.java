package de.neuchels.webshop.backend.service;

import de.neuchels.webshop.backend.entity.ProductEntity;
import de.neuchels.webshop.backend.exception.IdNotFoundException;
import de.neuchels.webshop.backend.model.OrderPositionResponse;
import de.neuchels.webshop.backend.repository.OrderPositionRepository;
import de.neuchels.webshop.backend.repository.OrderRepository;
import de.neuchels.webshop.backend.repository.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class ShoppingCartServiceTest {

    private ProductRepository productRepository;
    private ShoppingCartService shoppingCartService;

    @BeforeEach
    public void setUp() {
        productRepository = mock(ProductRepository.class);
        shoppingCartService = new ShoppingCartService(
                mock(OrderRepository.class),
                mock(OrderPositionRepository.class),
                productRepository
        );
    }

    @Test
    public void testThat_calculateSumForEmptyShoppingCart_returnsDeliveryCost() {
        // given

        // when
        Long result = shoppingCartService.calculateSumByShoppingCart(
                new ArrayList<OrderPositionResponse>(),
                500
        );

        // then
        Assertions.assertEquals(500, result);
    }

    @Test
    public void testThat_calculateSumForSumWithOneProduct_sumsPriceOfProduct() {
        // given
        ProductEntity savedProduct = new ProductEntity(
                UUID.randomUUID().toString(),
                "",
                "",
                1000,
                new ArrayList<>()
        );
        given(productRepository.findById(savedProduct.getId())).willReturn(Optional.of(savedProduct));

        List<OrderPositionResponse> orderPositions = new ArrayList<>();
        addOrderPosition(orderPositions, savedProduct,  1);

        // when
        Long result = shoppingCartService.calculateSumByShoppingCart(
                orderPositions,
                500
        );

        // then
        Assertions.assertEquals(1500, result);
    }

    @Test
    public void testThat_calculateSumForSumWithTwoProducts_sumsPricesOfProducts() {
        // given
        ProductEntity savedProduct1 = new ProductEntity(
                UUID.randomUUID().toString(),
                "",
                "",
                1000,
                new ArrayList<>()
        );
        given(productRepository.findById(savedProduct1.getId())).willReturn(Optional.of(savedProduct1));

        ProductEntity savedProduct2 = new ProductEntity(
                UUID.randomUUID().toString(),
                "",
                "",
                2000,
                new ArrayList<>()
        );
        given(productRepository.findById(savedProduct2.getId())).willReturn(Optional.of(savedProduct2));

        List<OrderPositionResponse> orderPositions = new ArrayList<>();
        addOrderPosition(orderPositions, savedProduct1,  1);
        addOrderPosition(orderPositions, savedProduct2,  4);

        // when
        Long result = shoppingCartService.calculateSumByShoppingCart(
                orderPositions,
                500
        );

        // then
        Assertions.assertEquals(9500, result);
    }

    @Test
    public void testThat_calculateSumForSumWithNotExistingProduct_throwsException() {
        // given
        ProductEntity notSavedProduct = new ProductEntity("", "", "", 1000, new ArrayList<>());

        List<OrderPositionResponse> orderPositions = new ArrayList<>();
        addOrderPosition(orderPositions, notSavedProduct,  1);

        // then
        Assertions.assertThrows(IdNotFoundException.class, () -> {
            // when
            Long result = shoppingCartService.calculateSumByShoppingCart(
                    orderPositions,
                    500
            );
        });
    }

    @Test
    public void testThat_calculateSumForSumWithNegativeQuantity_throwsException() {
        // given
        ProductEntity savedProduct1 = new ProductEntity(
                UUID.randomUUID().toString(),
                "",
                "",
                1000,
                new ArrayList<>()
        );
        given(productRepository.findById(savedProduct1.getId())).willReturn(Optional.of(savedProduct1));

        ProductEntity savedProduct2 = new ProductEntity(
                UUID.randomUUID().toString(),
                "",
                "",
                2000,
                new ArrayList<>()
        );
        given(productRepository.findById(savedProduct2.getId())).willReturn(Optional.of(savedProduct2));

        List<OrderPositionResponse> orderPositions = new ArrayList<>();
        addOrderPosition(orderPositions, savedProduct1,  1);
        addOrderPosition(orderPositions, savedProduct2,  -4);

        // then
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            // when
            Long result = shoppingCartService.calculateSumByShoppingCart(
                    orderPositions,
                    500
            );
        });
    }

    private void addOrderPosition(List<OrderPositionResponse> orderPositions, ProductEntity savedProduct, int quantity) {
        orderPositions.add(
                new OrderPositionResponse(
                        "1",
                        "order-id",
                        savedProduct.getId(),
                        quantity
                )
        );
    }

}
