package de.neuchels.webshop.backend.model

data class OrderUpdateRequest (
    val orderStatus: OrderStatus?
)
