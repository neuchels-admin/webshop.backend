package de.neuchels.webshop.backend.exception

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import javax.servlet.http.HttpServletRequest

@ControllerAdvice
class ApiExceptionHandler {

    @ExceptionHandler(Throwable::class)
    fun Exception.handleErrors(
        request: HttpServletRequest,
        exception: Throwable
    ): ResponseEntity<ErrorInfo> {
        //Log für den Server
        println(exception.message)
        println(exception)

        //Log für den Developer
        //Switch Case
        val (message, statusCode) = when (exception) {
            is IdNotFoundException -> exception.message to HttpStatus.BAD_REQUEST
            is WebshopException -> exception.message to exception.statusCode
            is IllegalArgumentException -> (exception.message ?: "Illegal Argument") to HttpStatus.BAD_REQUEST
            is JpaObjectRetrievalFailureException -> (exception.message ?: "Object not found") to HttpStatus.BAD_REQUEST
            else -> (exception.message ?: "An error occured") to HttpStatus.INTERNAL_SERVER_ERROR
        }
        val errorInfo = ErrorInfo(message, request.requestURI)
        return ResponseEntity(errorInfo, statusCode)
    }
}

data class ErrorInfo(
    val error: String,
    val path: String
)

// Immer wenn eine Exception in den Controllern durchrauscht und im Spring Framework landet, dann führe den Exception Handler aus.
// @ControllerAdvice Diese Annotation sagt Spring, dass diese Klasse an jeden Controller angeflanscht wird.