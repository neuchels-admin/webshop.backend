package de.neuchels.webshop.backend.repository;

import de.neuchels.webshop.backend.entity.ProductEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<ProductEntity, String> {
}