package de.neuchels.webshop.backend.model

data class CustomerResponse(
    val id: String,
    val firstName: String,
    val lastName: String,
    var email: String
)