REST ist eine Art Nutzungskonzept, wie wir
die vier Bausteine einsetzen sollten, um APIs (Endepunkte) zu designen
- Pfad
- Query Paramter
- Http Verb GET, POST, PUT, DELETE (HEAD, OPTIONS, PATCH, TRACE)
- Request Body (z. B. Formulardaten)

REST: Ressourcen (Entitys):
Produkte
Kunden
Bestellungen
=> wir haben nicht mehr den Unteschied createProducts usw. sondern wir 
nutzen die verschiedenen Http Verben für die Beschreibung, ob ich etwas 
lesen möchte oder etwas schreiben möchte

Lade alle Produkte vom Server:
Schritt 1 GET /api/products
Schritt 2 GET /api/products

Lade Produkt mit bestimmtem Tag
(Schritt 1 nicht möglich, da der Server nicht erkennen kann, ob es sich um eine id oder einen tag handelt)
Schritt 1 GET /api/products/{tag}
Schritt 2 GET /api/products?tag={tag}

Erzeuge neues Produkt
Schritt 1 POST /api/createProduct
Schritt 2 POST /api/products

Lösche Produkt
Schritt 1 DELETE /api/products/{id}

Lade Produkt mit bestimmter ID
Schritt 1 GET /api/products/{id}

Ändere Preis eines Produkts
Schritt 1 POST /api/updatePrice
Schritt 2 PUT /api/products/{id}/price

Update Produktbeschreibung
Schritt 1 PUT /api/products/{id}/description

Update Produkt
(FAVORIT; Update des gesamten Produkts)
Schritt 1 PUT /api/products/{id}

Füge Tags zu Produkt hinzu
(hier ggfs. nicht Update Produkt nutzen, da die Implementierung umfangreicher ist)
Schritt 1 POST /api/addTags
Schritt 2 PUT /api/products/{id}/tags

Bestelle Produkt ==> Erzeuge neue Bestellung
(Falle: wir schreiben einen Endpunkt für eine Operation statt für eine Ressource)
Schritt 1: POST /api/product/{id}/orderProduct
Schritt 2: POST /api/orders

Füge Produkt zur Bestellung hinzu
PUT /api/orders/{id}/products (Sub-Resource)
oder
PUT /api/orders/{id}



