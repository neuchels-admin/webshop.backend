package de.neuchels.webshop.backend.entity

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name="order_positions")
class OrderPositionEntity(
    @Id val id: String,
    val orderId: String,
    val productId: String,
    val quantity: Long
)