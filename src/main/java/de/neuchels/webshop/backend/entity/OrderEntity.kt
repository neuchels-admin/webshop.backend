package de.neuchels.webshop.backend.entity

import de.neuchels.webshop.backend.model.OrderStatus

import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name="orders")
class OrderEntity (
    @Id val id: String,
    val customerId: String,
    val orderTime: LocalDateTime,
    @Enumerated(EnumType.STRING)
    val status: OrderStatus
)