package de.neuchels.webshop.backend.service

import de.neuchels.webshop.backend.entity.OrderEntity
import de.neuchels.webshop.backend.entity.ProductEntity
import de.neuchels.webshop.backend.exception.IdNotFoundException
import de.neuchels.webshop.backend.model.OrderPositionResponse
import de.neuchels.webshop.backend.model.ShoppingCartResponse
import de.neuchels.webshop.backend.repository.OrderPositionRepository
import de.neuchels.webshop.backend.repository.OrderRepository
import de.neuchels.webshop.backend.repository.ProductRepository
import org.springframework.stereotype.Service

@Service
class ShoppingCartService(
    val orderRepository : OrderRepository,
    val orderPositionRepository: OrderPositionRepository,
    val productRepository: ProductRepository
) {
    fun getShoppingCartByCustomerId(customerId: String): ShoppingCartResponse {

        val orders: List<OrderEntity> = orderRepository.findAllByCustomerIdWhereOrderStatusIsNew(customerId)
        val orderIds = orders.map { it.id }
        val orderPositions = orderPositionRepository.findAllById(orderIds).map { OrderService.mapToResponse(it) }

        val deliveryCost = 800L //TODO: feature to select delivery method?
        val totalAmount = calculateSumByShoppingCart(orderPositions, deliveryCost)

        return ShoppingCartResponse(
            customerId = customerId,
            orderPositions = orderPositions,
            deliveryCostInCent = deliveryCost,
            deliveryOption = "STANDARD",
            totalAmountInCent = totalAmount
        )
    }

    fun calculateSumByShoppingCart(
        orderPositions: List<OrderPositionResponse>,
        deliveryCost: Long
    ): Long {
        val positionAmounts: List<Long> = orderPositions.map {
            val product: ProductEntity = productRepository
                .findById(it.productId)
                .orElseThrow { throw IdNotFoundException("Product with id ${it.productId} not found") }
            if (it.quantity <= 0)
                throw java.lang.IllegalArgumentException("OrderPosition with ${it.quantity} is not allowed");
            it.quantity * product.priceInCent
        }
        val positionSum = positionAmounts.sumOf { it.toInt() }
        return positionSum + deliveryCost
    }
}
