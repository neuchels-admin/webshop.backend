package de.neuchels.webshop.backend.exception

import org.springframework.http.HttpStatus
import java.lang.RuntimeException


data class WebshopException(
    override val message: String,
    val statusCode: HttpStatus
): RuntimeException(message) {
}