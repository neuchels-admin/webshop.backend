package de.neuchels.webshop.backend.repository

import de.neuchels.webshop.backend.entity.OrderPositionEntity
import org.springframework.data.jpa.repository.JpaRepository

interface OrderPositionRepository: JpaRepository<OrderPositionEntity, String> {
}

