package de.neuchels.webshop.backend.exception

import org.springframework.http.HttpStatus
import java.lang.RuntimeException


data class IdNotFoundException(
    override val message: String,
    val statusCode: HttpStatus = HttpStatus.BAD_REQUEST
): RuntimeException(message) {
}