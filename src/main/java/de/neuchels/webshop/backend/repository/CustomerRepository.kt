package de.neuchels.webshop.backend.repository

import de.neuchels.webshop.backend.entity.CustomerEntity
import org.springframework.data.jpa.repository.JpaRepository
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

interface CustomerRepository: JpaRepository<CustomerEntity, String> {
}

