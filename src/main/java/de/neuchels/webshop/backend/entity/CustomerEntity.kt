package de.neuchels.webshop.backend.entity

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name="customers")
class CustomerEntity(
    @Id val id: String,
    val firstName: String,
    val lastName: String,
    val email: String
)