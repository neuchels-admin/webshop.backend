package de.neuchels.webshop.backend.model

data class GetOrderPositionResponse (
    val id: String,
    val product: ProductResponse,
    val quantity: Long
)