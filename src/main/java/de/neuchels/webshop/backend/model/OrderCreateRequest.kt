package de.neuchels.webshop.backend.model

data class OrderCreateRequest (
    var customerId: String
)
