package de.neuchels.webshop.backend.service

import de.neuchels.webshop.backend.entity.OrderEntity
import de.neuchels.webshop.backend.entity.OrderPositionEntity
import de.neuchels.webshop.backend.exception.IdNotFoundException
import de.neuchels.webshop.backend.model.*
import de.neuchels.webshop.backend.repository.*
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.UUID

@Service
class OrderService(
    val orderRepository: OrderRepository,
    val orderPositionRepository: OrderPositionRepository,
    val customerRepository: CustomerRepository,
    val productRepository: ProductRepository
) {
    fun createOrder(request: OrderCreateRequest): OrderResponse {
        customerRepository.findById(request.customerId)

        val orderEntity = OrderEntity (
            id = UUID.randomUUID().toString(),
            customerId = request.customerId,
            orderTime = LocalDateTime.now(),
            status = OrderStatus.NEW)

        val savedOrder = orderRepository.save(orderEntity)
        return mapToResponse(savedOrder)
    }

    fun createNewPositionForOrder(
        orderId: String,
        request: OrderPositionCreateRequest
    ): OrderPositionResponse {

        orderRepository.findById(orderId)
            ?: throw IdNotFoundException(
                message = "Order with $orderId not found",
                statusCode = HttpStatus.BAD_REQUEST
            )

        if (productRepository.findById(request.productId).isEmpty)
            throw IdNotFoundException(
                message = "Product with ${request.productId} not found",
                statusCode = HttpStatus.BAD_REQUEST
            )

        val orderPositionEntity = de.neuchels.webshop.backend.entity.OrderPositionEntity(
            id = UUID.randomUUID().toString(),
            orderId = orderId,
            productId = request.productId,
            quantity = request.quantity
        )
        val savedOrderPosition = orderPositionRepository.save(orderPositionEntity)

        return mapToResponse(savedOrderPosition)
    }

    fun updateOrder(id: String, request: OrderUpdateRequest): OrderResponse {
        val order = orderRepository.getById(id)

        val updatedOrder = OrderEntity (
            id = order.id,
            customerId = order.customerId,
            orderTime = order.orderTime,
            status = request.orderStatus ?: order.status
        )

        val savedOrder = orderRepository.save(updatedOrder)
        return mapToResponse(savedOrder)
    }

    private fun mapToResponse(savedOrder: OrderEntity) = OrderResponse(
        id = savedOrder.id,
        customerId = savedOrder.customerId,
        orderTime = savedOrder.orderTime,
        status = savedOrder.status,
        orderPositions = emptyList()
    )

    fun getOrder(id: String): GetOrderResponse {
        val order = orderRepository.getById(id)
        val customer = customerRepository.getById(order.customerId)
        val positions = orderPositionRepository.findAll()
            .filter { it.orderId == order.id }
            .map {
                val productEntity = productRepository.getById(it.productId)
                GetOrderPositionResponse(
                    id = it.id,
                    quantity = it.quantity,
                    product = ProductResponse(
                        productEntity.id,
                        productEntity.name,
                        productEntity.description,
                        productEntity.priceInCent,
                        productEntity.tags
                    )
                )
            }

        return GetOrderResponse(
            id = order.id,
            orderTime =  order.orderTime,
            status = order.status,
            customer = CustomerResponse(
                id = customer.id,
                firstName = customer.firstName,
                lastName = customer.lastName,
                email = customer.email
            ),
            orderPositions = positions
        )
    }

    companion object {
        fun mapToResponse(savedOrderPosition: OrderPositionEntity): OrderPositionResponse {
            return OrderPositionResponse(
                id = savedOrderPosition.id,
                orderId = savedOrderPosition.orderId,
                productId = savedOrderPosition.productId,
                quantity = savedOrderPosition.quantity
            )
        }
    }
}