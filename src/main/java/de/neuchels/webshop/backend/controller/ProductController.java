package de.neuchels.webshop.backend.controller;

import de.neuchels.webshop.backend.entity.ProductEntity;
import de.neuchels.webshop.backend.exception.IdNotFoundException;
import de.neuchels.webshop.backend.model.ProductCreateRequest;
import de.neuchels.webshop.backend.model.ProductResponse;
import de.neuchels.webshop.backend.model.ProductUpdateRequest;
import de.neuchels.webshop.backend.repository.ProductRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
public class ProductController {

    private final ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping("/products")
    public List<ProductResponse> getAllProducts(
            @RequestParam(required = false) String tag
    ) {
        return productRepository
                .findAll()
                .stream()
                .filter((productEntity) -> tag == null || productEntity.getTags().contains(tag))
                .map((productEntity) -> mapToResponse(productEntity))
                .collect(Collectors.toList());
    }

    @GetMapping("/products/{id}")
    public ProductResponse getProductById(
            @PathVariable String id
    ) {
        ProductEntity product = productRepository.getById(id);

        return mapToResponse(product);
    }

    @PostMapping("/products")
    public ProductResponse createProduct(
            @RequestBody ProductCreateRequest request
    ) {
        ProductEntity productEntity = new ProductEntity(
                UUID.randomUUID().toString(),
                request.getName(),
                request.getDescription(),
                request.getPriceInCent(),
                request.getTags()
        );

        ProductEntity savedProduct = productRepository.save(productEntity);

        return mapToResponse(savedProduct);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity deleteProduct(
            @PathVariable String id
    ) {
        productRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/products/{id}")
    public ProductResponse updateProduct(
            @PathVariable String id,
            @RequestBody ProductUpdateRequest request
    ) {
        final ProductEntity product = productRepository.findById(id)
                .orElseThrow(() ->
                        new IdNotFoundException(
                            "Product with id " + id + " not found",
                            HttpStatus.BAD_REQUEST
                        )
                );

        final ProductEntity updatedProduct = new ProductEntity(
                product.getId(),
                (request.getName() == null) ? product.getName() : request.getName(),
                (request.getDescription() == null) ? product.getDescription() : request.getDescription(),
                (request.getPriceInCent() == null) ? product.getPriceInCent() : request.getPriceInCent(),
                product.getTags()
        );

        ProductEntity savedProduct = productRepository.save(updatedProduct);

        return mapToResponse(savedProduct);
    }

    private ProductResponse mapToResponse(ProductEntity product) {
        return new ProductResponse(
                product.getId(),
                product.getName(),
                product.getDescription(),
                product.getPriceInCent(),
                product.getTags()
        );
    }
}
